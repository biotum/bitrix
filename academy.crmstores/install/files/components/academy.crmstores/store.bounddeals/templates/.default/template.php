<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

global $APPLICATION;

/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('currency')) {
    ShowError(Loc::getMessage('CRMSTORES_NO_CURRENCY_MODULE'));
    return;
}

$bodyClass = $APPLICATION->GetPageProperty('BodyClass');
$APPLICATION->SetPageProperty('BodyClass', ($bodyClass ? $bodyClass . ' ' : '') . 'disable-grid-mode');
$component=new CAcademyCrmStoresStoreBoundDealsComponent();
$rows = array();

foreach ($arResult['DEALS'] as $deal) {
    $dealUrl = CComponentEngine::makePathFromTemplate(
        Option::get('academy.crmstores', 'DEAL_DETAIL_TEMPLATE'),
        array('DEAL_ID' => $deal['DEAL_ID'])
    );
    $dealID = CCrmDeal::GetByID($deal['DEAL_ID']);
    $rows[] = array(
        'id' => $deal['STORE_ID'],
        'columns' => array(
            'ID' => $deal['STORE_ID'],
            'TITLE' => '<a href="' . htmlspecialcharsbx($dealUrl) . '">' .$dealID['TITLE']. '</a>',
            'CONSIGNEE'=>$component->CompanyName($deal['COMPANY_ID']),
            'PRODUCT'=>$deal['PRODUCT_NAME'],
            'WEIGHT'=>$deal['WEIGHT'],
            'DATE_DELIVERY'=> $deal['DATE_DELIVERY'],
            'TIME_DELIVERY'=> $deal['TIME_DELIVERY'],
            //'OPPORTUNITY' => CurrencyFormat($deal['OPPORTUNITY'], $deal['CURRENCY_ID'])
        )
    );
}

$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    array(
        'GRID_ID' => 'CRMSTORES_BOUND_DEALS',
        'HEADERS' => array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('CRMSTORES_DEAL_ID'),
                'type' => 'int',
                'default' => false,
            ),
            array(
                'id' => 'TITLE',
                'name' => Loc::getMessage('CRMSTORES_DEAL_TITLE'),
                'default' => true
            ),
            array(
                'id' => 'CONSIGNEE',
                'name' => Loc::getMessage('CRMSTORES_CONSIGNEE_TITLE'),
                'default' => true
            ),
            array(
                'id' => 'PRODUCT',
                'name' => Loc::getMessage('CRMSTORES_PRODUCT_TITLE'),
                'default' => true
            ),

            array(
                'id' => 'WEIGHT',
                'name' => Loc::getMessage('CRMSTORES_WEIGHT_TITLE'),
                'default' => true
            ),
            array(
                'id' => 'DATE_DELIVERY',
                'name' => Loc::getMessage('CRMSTORES_DATE_DELIVERY_TITLE'),
                'default' => true
            ),
            array(
                'id' => 'TIME_DELIVERY',
                'name' => Loc::getMessage('CRMSTORES_TIME_DELIVERY_TITLE'),
                'default' => true
            ),

        ),
        'ROWS' => $rows
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);


