<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMSTORES_NO_MODULE'] = 'Модуль "Пункты разгрузки" не установлен.';
$MESS['CRMSTORES_STORE_NOT_FOUND'] = 'Пункт разгрузки не существует.';
