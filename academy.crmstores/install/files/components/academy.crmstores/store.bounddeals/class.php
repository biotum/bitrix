<?php

use Academy\CrmStores\Entity\DeliveryProductTable;
use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Crm\DealTable;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') || die;

class CAcademyCrmStoresStoreBoundDealsComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        if (!Loader::includeModule('crm')) {
            ShowError(Loc::getMessage('CRMSTORES_NO_CRM_MODULE'));
            return;
        }

        if (!Loader::includeModule('academy.crmstores')) {
            ShowError(Loc::getMessage('CRMSTORES_NO_MODULE'));
            return;
        }

        if (intval($this->arParams['STORE_ID']) <= 0) {
            ShowError(Loc::getMessage('CRMSTORES_STORE_NOT_FOUND'));
            return;
        }

        $dbStore = StoreTable::getById($this->arParams['STORE_ID']);
        $store = $dbStore->fetch();

        if (empty($store)) {
            ShowError(Loc::getMessage('CRMSTORES_STORE_NOT_FOUND'));
            return;
        }


        $dbCompany = DeliveryProductTable::getList(array(
            'filter' => array(
                'STORE_ID' => $this->arParams['STORE_ID']
            )
        ));

        $dealDelivery = $dbCompany->fetchAll();




        $this->arResult = array(
           // 'STORE' => $store,
            'DEALS' =>  $dealDelivery,

        );

        $this->includeComponentTemplate();
    }


    public function CompanyName($id){
        $dbCompany = CompanyTable::getList(array('select' => array('TITLE'),
            'filter' => array(
                'ID' => $id
            )
        ));
        $name=$dbCompany->fetch();

        return $name['TITLE'];
    }

}