<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_DETAILS_URL_TEMPLATE'] = 'Шаблон пути к карточке Пункта разгрузки';
$MESS['CRMSTORES_EDIT_URL_TEMPLATE'] = 'Шаблон пути к форме редактирования Пункта разгрузки';