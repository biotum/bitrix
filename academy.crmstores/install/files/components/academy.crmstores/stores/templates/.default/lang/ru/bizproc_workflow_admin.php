<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NEW_BP_STATEMACHINE'] = 'Создать бизнес-процесс со статусами';
$MESS['CRMSTORES_NEW_BP_SEQUENTAL'] = 'Создать последовательный бизнес-процесс';
$MESS['CRMSTORES_BACK_TO_LIST'] = '&ltrif;&nbsp; К пунктам разгрузки';
$MESS['CRMSTORES_BP_LIST_TITLE'] = 'Список шаблонов: Пункты разгрузки';