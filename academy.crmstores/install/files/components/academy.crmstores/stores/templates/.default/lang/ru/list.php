<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_LIST_TITLE'] = 'Пункты разгрузки';
$MESS['CRMSTORES_ADD'] = 'Добавить';
$MESS['CRMSTORES_CONFIGURE_WORKFLOWS'] = 'Настроить бизнес-процессы';