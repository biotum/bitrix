<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_MODULE'] = 'Модуль "Пункты разгрузки" не установлен.';
$MESS['CRMSTORES_STORE_NOT_FOUND'] = 'Пункт разгрузки не существует.';
$MESS['CRMSTORES_SHOW_TITLE_DEFAULT'] = 'Пункт разгрузки';
$MESS['CRMSTORES_SHOW_TITLE'] = 'Пункт разгрузки №#ID# &mdash; #NAME#';
$MESS['CRMSTORES_ERROR_EMPTY_NAME'] = 'Название Пункта разгрузки не задано.';
$MESS['CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID'] = 'Не указан ответственный.';
$MESS['CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID'] = 'Указанный ответственный сотрудник не существует.';