<?php

use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Crm\DealTable;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') || die;

class CAcademyCrmStoresStoreBoundCompanyComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        if (!Loader::includeModule('crm')) {
            ShowError(Loc::getMessage('CRMSTORES_NO_CRM_MODULE'));
            return;
        }

        if (!Loader::includeModule('academy.crmstores')) {
            ShowError(Loc::getMessage('CRMSTORES_NO_MODULE'));
            return;
        }

        if (intval($this->arParams['STORE_ID']) <= 0) {
            ShowError(Loc::getMessage('CRMSTORES_STORE_NOT_FOUND'));
            return;
        }

        $dbStore = StoreTable::getById($this->arParams['STORE_ID']);
        $store = $dbStore->fetch();

        if (empty($store)) {
            ShowError(Loc::getMessage('CRMSTORES_STORE_NOT_FOUND'));
            return;
        }

        $dealUfName = Option::get('academy.crmstores', 'DEAL_UF_NAME');

        $dbCompany = CompanyTable::getList(array(
            'filter' => array(
                'UF_CRM_COMPANY_TEST' => $store['ID']
            )
        ));

        $company = $dbCompany->fetchAll();

        $this->arResult = array(
            'STORE' => $store,
            'COMPANY'=>$company,
        );

        $this->includeComponentTemplate();
    }
}