<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

global $APPLICATION;

/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('currency')) {
    ShowError(Loc::getMessage('CRMSTORES_NO_CURRENCY_MODULE'));
    return;
}

$bodyClass = $APPLICATION->GetPageProperty('BodyClass');
$APPLICATION->SetPageProperty('BodyClass', ($bodyClass ? $bodyClass . ' ' : '') . 'disable-grid-mode');

$rows1 = array();

foreach ($arResult['COMPANY'] as $company) {

    $dealUrl = CComponentEngine::makePathFromTemplate(
        Option::get('academy.crmstores', 'COMPANY_DETAIL_TEMPLATE'),
        array('ID' => $company['ID'])
    );

    $rows1[] = array(
        'id' => $company['ID'],
        'columns' => array(
            'ID' => $company['ID'],
            'TITLE' => '<a href="' . htmlspecialcharsbx($dealUrl) . '">' . $company['TITLE'] . '</a>',
            'OPPORTUNITY' => CurrencyFormat($company['OPPORTUNITY'], $company['CURRENCY_ID'])
        )
    );
}

$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    array(
        'GRID_ID' => 'CRMSTORES_BOUND_DEALS',
        'HEADERS' => array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('CRMSTORES_COMPANY_ID'),
                'type' => 'int',
                'default' => true,
            ),
            array(
                'id' => 'TITLE',
                'name' => Loc::getMessage('CRMSTORES_COMPANY_TITLE'),
                'default' => true
            ),
//            array(
//                'id' => 'OPPORTUNITY',
//                'name' => Loc::getMessage('CRMSTORES_DEAL_OPPORTUNITY'),
//                'default' => true,
//            )
        ),
        'ROWS' => $rows1
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

