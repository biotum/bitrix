<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMBIDS_FIELD_ID'] = 'ID Доставки';
$MESS['CRMDELIVERY_FIELD_DEAL_ID'] = 'ID Сделки';
$MESS['CRMDELIVERY_FIELD_STORE_ID'] = 'ID Пункта разгрузки';
$MESS['CRMDELIVERY_FIELD_COMPANY_ID'] = 'ID Покупателя';
$MESS['CRMDELIVERY_FIELD_PRODUCT_NAME'] = 'Наименование продукта';
$MESS['CRMDELIVERY_FIELD_WEIGHT'] = 'Вес продукта';
$MESS['CRMDELIVERY_FIELD_DATE_DELIVERY'] = 'Дата доставки';
$MESS['CRMDELIVERY_FIELD_TIME_DELIVERY'] = 'Время доставки';


$MESS['CRMBIDS_FIELD_ASSIGNED_BY_ID'] = 'Ответственный';

$MESS['CRMBIDS_GROUP_AUTHOR'] = 'Ответственный';