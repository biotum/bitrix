<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_EVENT_ONCRMSTOREADD_NAME'] = 'Создание торговой точки';
$MESS['CRMSTORES_EVENT_ONCRMSTOREADD_DESCR'] = 'Событие, вызываемое при создании торговой точки.';
$MESS['CRMSTORES_EVENT_ONCRMSTOREUPDATE_NAME'] = 'Изменение торговой точки';
$MESS['CRMSTORES_EVENT_ONCRMSTOREUPDATE_DESCR'] = 'Событие, вызываемое при изменении торговой точки.';
$MESS['CRMSTORES_EVENT_ONCRMSTOREDELETE_NAME'] = 'Удаление торговой точки';
$MESS['CRMSTORES_EVENT_ONCRMSTOREDELETE_DESCR'] = 'Событие, вызываемое при удалении торговой точки.';