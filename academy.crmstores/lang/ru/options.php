<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_TAB_GENERAL_NAME'] = 'Общие настройки';
$MESS['CRMSTORES_TAB_GENERAL_TITLE'] = 'Настройки модуля «Пункт разгрузки CRM»';
$MESS['CRMSTORES_OPTION_DEAL_DETAIL_TEMPLATE'] = 'Шаблон URL карточки сделки';
$MESS['CRMSTORES_OPTION_STORE_DETAIL_TEMPLATE'] = 'Шаблон URL карточки Пункта разгрузки';
$MESS['CRMSTORES_OPTION_DEAL_UF_NAME'] = 'Название пользовательского поля сделки для связи с Пунктом разгрузки';
$MESS['CRMSTORES_OPTION_COMPANY_DETAIL_TEMPLATE'] = 'Шаблон URL компании';
$MESS['CRMSTORES_OPTION_STORE_COMPANY_TEMPLATE'] = 'Шаблон URL карточки Пункта разгрузки';
$MESS['CRMSTORES_OPTION_COMPANY_UF_NAME'] = 'Название пользовательского поля привязки с Пунктом разгрузки';