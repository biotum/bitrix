<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['ACADEMY_CRMSTORES.MODULE_NAME'] = 'Пункты разгрузки';
$MESS['ACADEMY_CRMSTORES.MODULE_DESC'] = 'Управление Пунктами разгрузки';
$MESS['ACADEMY_CRMSTORES.PARTNER_NAME'] = 'Касьянов В.П';
$MESS['ACADEMY_CRMSTORES.PARTNER_URI'] = 'https://academy.1c-bitrix.ru/';
