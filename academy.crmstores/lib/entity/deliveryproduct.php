<?php

namespace Academy\CrmStores\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class DeliveryProductTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'academy_crmdelivery_product';
    }

    public static function getMap()
    {
        global $DB;
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new IntegerField('DEAL_ID'),
            new IntegerField('STORE_ID'),
            new IntegerField('COMPANY_ID'),
            new StringField('PRODUCT_NAME'),
            new StringField('WEIGHT'),
            new StringField('DATE_DELIVERY'),
            new StringField('TIME_DELIVERY'),
            new IntegerField('ASSIGNED_BY_ID'),
//            new ReferenceField(
//                'ASSIGNED_BY',
//                UserTable::getEntity(),
//                array('=this.ASSIGNED_BY_ID' => 'ref.ID')
//            )
        );
    }
}
