<?php

namespace Academy\CrmStores\Handler;

use Academy\CrmStores\Entity\StoreTable;
use Academy\CrmStores\Rest\EventRegistrator;
use Academy\CrmStores\Rest\RestApi;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Localization\Loc;

class Rest
{
    public static function registerInterface()
    {
        Loc::getMessage('REST_SCOPE_ACADEMY.CRMSTORES');

        $eventOnAdd = new Event(StoreTable::getEntity(), StoreTable::EVENT_ON_AFTER_ADD, array(), true);
        $eventOnUpdate = new Event(StoreTable::getEntity(), StoreTable::EVENT_ON_AFTER_UPDATE, array(), true);
        $eventOnDelete = new Event(StoreTable::getEntity(), StoreTable::EVENT_ON_AFTER_DELETE, array(), true);

        return array(
            'academy.crmstores' => array(
                'crm.store.list' => array(RestApi::class, 'getList'),
                'crm.store.add' => array(RestApi::class, 'add'),
                'crm.store.get' => array(RestApi::class, 'get'),
                'crm.store.update' => array(RestApi::class, 'update'),
                'crm.store.delete' => array(RestApi::class, 'delete'),
                'crm.deliveryproduct.add' => array(RestApi::class, 'addDelivery'),
                'crm.deliveryproduct.update' => array(RestApi::class, 'updateDelivery'),
                'crm.deliveryproduct.delete' => array(RestApi::class, 'deleteDelivery'),
                'crm.deliveryproduct.list' => array(RestApi::class, 'getListDelivery'),
                \CRestUtil::EVENTS => array(
                    'onCrmStoreAdd' => array('academy.crmstores', $eventOnAdd->getEventType(), array(RestApi::class, 'prepareEventData')),
                    'onCrmStoreUpdate' => array('academy.crmstores', $eventOnUpdate->getEventType(), array(RestApi::class, 'prepareEventData')),
                    'onCrmStoreDelete' => array('academy.crmstores', $eventOnDelete->getEventType(), array(RestApi::class, 'prepareEventData')),
                ),

                \CRestUtil::PLACEMENTS => array(
                    'CRM_STORE_DETAILS' => array()
                ),
            )
        );
    }
}
