<?php

namespace Academy\CrmStores\Rest;

use Academy\CrmStores\BizProc\StoreDocument;
use Academy\CrmStores\Entity\DeliveryProductTable;
use Academy\CrmStores\Entity\StoreTable;
use Academy\CrmStores\Util\RestUtil;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Rest\RestException;

class RestApi extends \IRestService
{
    //const LIST_LIMIT = 3;

    /**
     * @param array $params
     * @param int $start
     * @param \CRestServer $server
     * @return array
     * @throws RestException
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function getList($params, $start, $server)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }

        try {
            $getListParams = array();
            $result = array();
            if (is_array($params['filter'])) {
                $getListParams['filter'] = $params['filter'];
            }

            if (is_array($params['select'])) {
                $getListParams['select'] = $params['select'];
            }

            if (is_array($params['order'])) {
                $getListParams['order'] = $params['order'];
            }
            $total = StoreTable::getCount($getListParams['filter'] ?: array());
         $getListParams = array_merge($getListParams, self::getNavData($start, true));


          $dbStores = StoreTable::getList($getListParams);


            $result[]=$dbStores->fetchAll();

            $result['total'] = $total;
            return $result;

        } catch (\Exception $e) {
            throw new RestException($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @param int $start
     * @param \CRestServer $server
     * @return array
     * @throws RestException
     * @throws \Bitrix\Main\ArgumentException
     */
    public static function get($params, $start, $server)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }

        try {
            $getListParams = array();
            $result = array();
            if ($params['ID']) {
                $getListParams['filter'] =['ID'=>$params['ID']];
            }

            if (is_array($params['select'])) {
                $getListParams['select'] = $params['select'];
            }

            if (is_array($params['order'])) {
                $getListParams['order'] = $params['order'];
            }
            $total = StoreTable::getCount($getListParams['filter'] ?: array());
            $getListParams = array_merge($getListParams, self::getNavData($start, true));


            $dbStores = StoreTable::getList($getListParams);


            $result[]=$dbStores->fetchAll();


            return $result;

        } catch (\Exception $e) {
            throw new RestException($e->getMessage());
        }
    }


    public static function add($params, $start, $server)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }

        $result = StoreTable::add($params['fields']);

        if (!$result->isSuccess()) {
            throw new RestException(implode(', ', $result->getErrorMessages()));
        }

//        if (Loader::includeModule('bizproc')) {
//            \CBPDocument::AutoStartWorkflows(
//                StoreDocument::getComplexDocumentType(),
//                \CBPDocumentEventType::Create,
//                StoreDocument::getComplexDocumentId($result->getId()),
//                array(),
//                $errors
//            );
 //       }

        return $result->getId();
    }
//{"ID":"3","fields":{"NAME":"\u0410\u0411\u0417 poljh566"}}
    public static function update($params, $start, $server)
    {

        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }
       $result = StoreTable::update($params['ID'], $params['fields']);

    //$result = StoreTable::update($params['id'], $params['fields']);
       // ['ID'=>'3'], ['fields'=>["NAME"=>"АБЗ  rrrrrrrrrr",]]
//        if ($result->isSuccess() && Loader::includeModule('bizproc')) {
//            \CBPDocument::AutoStartWorkflows(
//                StoreDocument::getComplexDocumentType(),
//                \CBPDocumentEventType::Edit,
//                StoreDocument::getComplexDocumentId((int)$params['id']),
//                array(),
//                $errors
//            );
//        }
        if (!$result->isSuccess())
        {
            return $result->getErrorMessages();
        }else{
            return $result->isSuccess();
        }

    }

    public static function delete($params, $start, $server)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }

        $result = StoreTable::delete($params['id']);
        return $result->isSuccess();
    }

    public static function addDelivery($params, $start, $server)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }
//          if($params['fields']=="DATE_DELIVERY"){
//              $date = new \Date($params['fields']['DATE_DELIVERY']);
//
//              $params['fields']['DATE_DELIVERY']=$date;
//          }
        $result = DeliveryProductTable::add($params['fields']);

        if (!$result->isSuccess()) {
            throw new RestException(implode(', ', $result->getErrorMessages()));
        }

//        if (Loader::includeModule('bizproc')) {
//            \CBPDocument::AutoStartWorkflows(
//                StoreDocument::getComplexDocumentType(),
//                \CBPDocumentEventType::Create,
//                StoreDocument::getComplexDocumentId($result->getId()),
//                array(),
//                $errors
//            );
        //       }

        return $result->getId();
    }

    public static function updateDelivery($params, $start, $server)
    {

        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }
        $result = DeliveryProductTable::update($params['ID'], $params['fields']);

        //$result = StoreTable::update($params['id'], $params['fields']);
        // ['ID'=>'3'], ['fields'=>["NAME"=>"АБЗ  rrrrrrrrrr",]]
//        if ($result->isSuccess() && Loader::includeModule('bizproc')) {
//            \CBPDocument::AutoStartWorkflows(
//                StoreDocument::getComplexDocumentType(),
//                \CBPDocumentEventType::Edit,
//                StoreDocument::getComplexDocumentId((int)$params['id']),
//                array(),
//                $errors
//            );
//        }

        return $result->isSuccess();
    }

    public static function deleteDelivery($params, $start, $server)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }

        $result = DeliveryProductTable::delete($params['id']);
        return $result->isSuccess();
    }

    public static function getListDelivery($params, $start, $server)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            throw new RestException(Loc::getMessage('CRMSTORES_NO_MODULE'), 'no_module');
        }

        try {
            $getListParams = array();

            if (is_array($params['filter'])) {
                $getListParams['filter'] = $params['filter'];
            }

            if (is_array($params['select'])) {
                $getListParams['select'] = $params['select'];
            }

            if (is_array($params['order'])) {
                $getListParams['order'] = $params['order'];
            }

            $getListParams = array_merge($getListParams, self::getNavData($start, true));

            $dbStores = DeliveryProductTable::getList($getListParams);

            return  $dbStores->fetchAll();

        } catch (\Exception $e) {
            throw new RestException($e->getMessage());
        }
    }

//    public static function prepareEventData($arguments, $handler)
//    {
//        /** @var Event $event */
//        $event = reset($arguments);
//
//        return $event->getParameters();
//    }
}
