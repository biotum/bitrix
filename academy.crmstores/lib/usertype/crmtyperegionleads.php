<?php

namespace Academy\CrmStores\UserType;


use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Crm\DealRecurTable;
use Bitrix\Crm\DealTable;
use Bitrix\Crm\Entity\Deal;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserField\TypeBase;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Web\Uri;
use CCrmBizProc;
use CCrmComponentHelper;
use CCrmDeal;
use CCrmOwnerType;
use CCrmPerms;
use CCrmUserType;


class CrmTypeRegionleads extends TypeBase
{
    const USER_TYPE_ID = 'crmtyperegionleads';
    const REGION=[
        1=>['ID'=>1,'NAME'=>'Северная Америка'],
        2=>['ID'=>2,'NAME'=>'Латинская Америка'],
        3=>['ID'=>3,'NAME'=>'Южная Америка'],
        4=>['ID'=>4,'NAME'=>'Африка'],
        5=>['ID'=>5,'NAME'=>'Европа'],
        6=>['ID'=>6,'NAME'=>'Азия'],
    ];
    function GetUserTypeDescription ()
    {
        return array(
            'USER_TYPE_ID' => static::USER_TYPE_ID,
            'CLASS_NAME' => __CLASS__,
            'DESCRIPTION' =>'РЕГИОН',
            'BASE_TYPE' => \CUserTypeManager::BASE_TYPE_INT,
            'EDIT_CALLBACK' => array(__CLASS__, 'GetPublicEdit'),
            'VIEW_CALLBACK' => array(__CLASS__, 'GetPublicView'),

        );
    }

    function GetDBColumnType ($arUserField)
    {
        global $DB;
        switch(strtolower($DB->type))
        {
            case "mysql":
                return "int(18)";
            case "oracle":
                return "number(18)";
            case "mssql":
                return "int";
        }
        return "int";
    }

    function GetFilterHTML($arUserField, $arHtmlControl)
    {
        return sprintf(
            '<input type="text" name="%s" size="%s" value="%s">',
            $arHtmlControl['NAME'],
            $arUserField['SETTINGS']['SIZE'],
            $arHtmlControl['VALUE']
        );

    }

    function GetFilterData($arUserField, $arHtmlControl)
    {
        return array(
        'id' => $arHtmlControl['ID'],
        'name' => $arHtmlControl['NAME'],
        'filterable' => ''
    );
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        return !empty($arHtmlControl['VALUE']) ? self::getStoreLink($arHtmlControl['VALUE']) : '&nbsp;';
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    public static function GetPublicView($arUserField, $arAdditionalParameters = array())
    {

        return !empty($arUserField['VALUE']) ? self::getStoreLink($arUserField['VALUE']) : '&nbsp;';
    }

    public static function GetPublicEdit($arUserField, $arAdditionalParameters = array())
    {
        $fieldName = static::getFieldName($arUserField, $arAdditionalParameters);
        $value = static::getFieldValue($arUserField, $arAdditionalParameters);
        $value = reset($value);


        return self::getStoreSelector($fieldName, $value);
    }

    function OnSearchIndex($arUserField)
    {
        if(is_array($arUserField["VALUE"]))
            return implode("\r\n", $arUserField["VALUE"]);
        else
            return $arUserField["VALUE"];
    }

    private static function getStoreSelector($fieldName, $fieldValue = [])
    {
        if (!Loader::includeModule('academy.crmstores')) {
            return '';
        }
        if (!Loader::includeModule('crm')) {
            return '';
        }
        global $APPLICATION;
        $vars=$GLOBALS["USER_FIELD_MANAGER"]->GetUserFields ('CRM_LEAD',
            preg_replace('~\D+~','',$APPLICATION->GetCurPageParam()));

        $isNoValue = $fieldValue === null;
Debug::dump($vars['UF_CRM_1576763679']['VALUE']);

        ob_start();
        ?>
          <?if($vars['UF_CRM_1576763679']['VALUE']==1):?>
            <div class="crm-offer-info-data-wrap">
        <? foreach (static::REGION as $store): ?>
            <?
            $selected = $store['ID'] == $fieldValue ? 'checked' : '';
            ?>
                <input class="crm-offer-radio" type="radio" id="opened_chbx" name="<?= $fieldName ?>" value="<?= $store['ID'] ?>" <?= $selected ?>>
                <label class="crm-offer-label" for="opened_chbx"><?= $store['NAME'] ?></label>
            <br>

        <? endforeach; ?>
            </div>

        <?endif;?>

        <?
        $selectorHtml = ob_get_clean();


        return $selectorHtml;
        }


    private static function getStoreLink($storeId)
    {


        if (!Loader::includeModule('academy.crmstores')) {
            return '';
        }

           echo '<div>' .htmlspecialcharsbx(static::REGION [$storeId]['NAME']). '</div>';




        return false;

    }

    static function GetListValues($arValues){
        $result=[];
        if(is_array($arValues['ID'])&& count($arValues['ID'])>0){
            $query = StoreTable::getlist([
                'filter'=>[
                    '=ID'=>$arValues['ID']
                ],
                'select'=>[
                    'ID',
                    'NAME',
                ]
            ]);
            while($data = $query->fetch()){
                $result[$data['ID']]['VALUE'] = $data['NAME'];
            }
        }
        return $result;
    }

    private static function getIDEdit()
    {
        $uri = new Uri($_SESSION['LOCAL_REDIRECTS']['R']);
        $redirect = preg_replace('~\D+~','',$uri->getPath());
        return  $redirect;
    }

}