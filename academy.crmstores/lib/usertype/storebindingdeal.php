<?php

namespace Academy\CrmStores\UserType;


use Academy\CrmStores\Entity\DeliveryProductTable;
use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Crm\DealRecurTable;
use Bitrix\Crm\DealTable;
use Bitrix\Crm\Entity\Deal;
use Bitrix\Intranet\Integration\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\DateField;
use Bitrix\Main\UserField\TypeBase;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Web\Uri;
use Bitrix\Tasks\Util\Entity\DateTimeField;
use CBitrixComponent;
use CCrmBizProc;
use CCrmComponentHelper;
use CCrmDeal;
use CCrmOwnerType;
use CCrmPerms;
use CCrmUserType;

use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\UserTable;

class StoreBindingdeal extends TypeBase
{
    const USER_TYPE_ID = 'storebindingdeal';

    function GetUserTypeDescription ()
    {
        return array(
            'USER_TYPE_ID' => static::USER_TYPE_ID,
            'CLASS_NAME' => __CLASS__,
            'DESCRIPTION' => Loc::getMessage('CRMSTORES_STOREBINDINGDEAL'),
            'BASE_TYPE' => \CUserTypeManager::BASE_TYPE_INT,
            'EDIT_CALLBACK' => array(__CLASS__, 'GetPublicEdit'),
            'VIEW_CALLBACK' => array(__CLASS__, 'GetPublicView'),

        );
    }

    function GetDBColumnType ($arUserField)
    {
        global $DB;
        switch(strtolower($DB->type))
        {
            case "mysql":
                return "int(18)";
            case "oracle":
                return "number(18)";
            case "mssql":
                return "int";
        }
        return "int";
    }

    function GetFilterHTML($arUserField, $arHtmlControl)
    {
        return sprintf(
            '<input type="text" name="%s" size="%s" value="%s">',
            $arHtmlControl['NAME'],
            $arUserField['SETTINGS']['SIZE'],
            $arHtmlControl['VALUE']
        );
    }

    function GetFilterData($arUserField, $arHtmlControl)
    {
        return array(
            'id' => $arHtmlControl['ID'],
            'name' => $arHtmlControl['NAME'],
            'filterable' => ''
        );
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        return !empty($arHtmlControl['VALUE']) ? self::getStoreLink($arHtmlControl['VALUE']) : '&nbsp;';
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    public static function GetPublicView($arUserField, $arAdditionalParameters = array())
    {

        $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", self::getIDEdit());
        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
            'filter' => array(
                'ID' => $consignee['UF_CRM_1566456322']['VALUE']
            )
        ));
        $stores2 = $dbCompany->fetchAll();
        $dbStores1 = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => $stores2[0]['UF_CRM_COMPANY_TEST'])
            ));
        $stores3 = $dbStores1->fetchAll();
        $param = implode(",", $consignee['UF_CRM_STORE_DEAL']['VALUE']);
        $dbStores = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => explode(',', $param))
            ));
        $stores = $dbStores->fetchAll();

        $delivery=DeliveryProductTable::getList(
            array('select' => array('*'),
                'filter' => array("=COMPANY_ID" =>$consignee['UF_CRM_1566456322']['VALUE'],"DEAL_ID"=>self::getIDEdit())
            )
        );
        $delivery_result=$delivery->fetchAll();


        ?>


        <table class="table information_json" border="1">
            <tr>
                <th>Пункт разгрузки</th>
                <th>Вес</th>
                <th>Срок доставки</th>
<!--                <th>Время поставки</th>-->
            </tr>
            <tr class="information_json_plus">

            </tr>
            <? foreach ($delivery_result as $delivery):
                $storeDetailTemplate = Option::get('academy.crmstores', 'STORE_DETAIL_TEMPLATE');
                $storeUrl = \CComponentEngine::makePathFromTemplate(
                    $storeDetailTemplate,
                    array('STORE_ID' =>$delivery['STORE_ID'])
                );
                ?>
                <tr>
                    <td  class="form-control name" style="min-width: 400px;" >
                        <!--                   <input type="text"  class="form-control name" style="min-width: 400px;"  value="--><?//=self::getStoreInfi($delivery['STORE_ID'])?><!--" readonly >-->
                        <a class="feed-com-add-link" href="<?=htmlspecialcharsbx($storeUrl)?>"><?=self::getStoreInfi($delivery['STORE_ID'])?></a>
                    </td>
                    <td><input type="text" name="WEIGHT" data-id="<?= $delivery['ID']?>" class="form-control  weight" style="width: 50px;" id="information_json_val[w<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['WEIGHT']?>" placeholder="Вес в тоннах" readonly ></td>
                    <td><input type="text" name="DATE" data-id="<?=  $delivery['ID']?>" class="form-control  date" style="width: 200px;" id="information_json_val[d<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['DATE_DELIVERY']?>" placeholder="Срок доставки" readonly ></td>
<!--                    <td><input type="time" name="TIME" data-id="--><?//= $delivery['ID']?><!--" class="form-control time" style="width: 100px;" id="information_json_val[t--><?//=$delivery['STORE_ID']?><!--]"  value="--><?//= $delivery['TIME_DELIVERY']?><!--" placeholder="Время доставки" readonly ></td>-->
                </tr>
            <? endforeach; ?>

        </table>
        <?

        return false;
       // return 'gfhgfhhdfhdfhdhhd';//!empty($arUserField['VALUE']) ? self::getStoreLink($arUserField['VALUE']) : '&nbsp;';
    }

    public static function GetPublicEdit($arUserField, $arAdditionalParameters = array())
    {
        $fieldName = static::getFieldName($arUserField, $arAdditionalParameters);
        $value = static::getFieldValue($arUserField, $arAdditionalParameters);
        $value = reset($value);


        return self::getStoreSelector($fieldName, $value);
    }

    function OnSearchIndex($arUserField)
    {
        if(is_array($arUserField["VALUE"]))
            return implode("\r\n", $arUserField["VALUE"]);
        else
            return $arUserField["VALUE"];
    }

    private static function getStoreSelector($fieldName, $fieldValue = null)
    {
        \CJSCore::Init(array("jquery","date"));
        if (!Loader::includeModule('academy.crmstores')) {
            return '';
        }
        global $APPLICATION;

        $dealID = CCrmDeal::GetByID(self::getIDEdit());

        $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", self::getIDEdit());

        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
            'filter' => array(
                'ID' => $consignee['UF_CRM_1566456322']['VALUE']
            )
        ));

        $stores2 = $dbCompany->fetchAll();
        $dbStores1 = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => $stores2[0]['UF_CRM_COMPANY_TEST'])
            ));
        $stores3 = $dbStores1->fetchAll();


        $param = implode(",", $consignee['UF_CRM_STORE_DEAL']['VALUE']);

            $dbStores = StoreTable::getList(
                array('select' => array('ID', 'NAME','ADDRESS'),
                 'filter' => array("=ID" => explode(',', $param))
                ));

        $stores = $dbStores->fetchAll();

       $delivery=DeliveryProductTable::getList(
           array('select' => array('*'),
              'filter' => array("=COMPANY_ID" =>$consignee['UF_CRM_1566456322']['VALUE'],"DEAL_ID"=>self::getIDEdit())
           )
       );
        $delivery_result=$delivery->fetchAll();

        $isNoValue = $fieldValue === null;
        ob_start();

        ?>

        <? if(CCrmDeal::GetByID(self::getIDEdit())):?>

           <table class="table information_json">
            <tr>
                <th>Пункт разгрузки</th>
                <th></th>
                <th>Вес</th>
                <th>Срок доставки</th>
<!--                <th>Время поставки</th>-->
            </tr>
               <tr class="information_json_plus">
               </tr>
                   <? foreach ($delivery_result as $delivery):
                   ?>
               <tr>
                   <td><input type="hidden" name="<?= $fieldName ?>" class="form-control store" style="min-width: 400px;" id="name[<?=$delivery['STORE_ID']?>]" value="<?=$delivery['STORE_ID']?>" readonly >
                   <input type="text"  class="form-control name" style="min-width: 400px;"  value="<?=self::getStoreInfi($delivery['STORE_ID'])?>" readonly ></td>
                   <td><span class="btn btn-danger minus pull-right" data-id="<?=  $delivery['ID']?>">&ndash;</span></td>
                       <td><input type="text" name="WEIGHT" data-id="<?= $delivery['ID']?>" class="form-control  weight" style="width: 50px;" id="information_json_val[w<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['WEIGHT']?>" placeholder="Вес в тоннах"></td>
                       <td><input type="text" name="DATE" onclick="BX.calendar({node: this, field: this, bTime: true});" data-id="<?=  $delivery['ID']?>" class="form-control  date" style="width: 200px;" id="information_json_val[d<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['DATE_DELIVERY']?>" placeholder="Срок доставки"></td>
<!--                       <td><input type="time" name="TIME" data-id="--><?//= $delivery['ID']?><!--" class="form-control time" style="width: 100px;" id="information_json_val[t--><?//=$delivery['STORE_ID']?><!--]"  value="--><?//= $delivery['TIME_DELIVERY']?><!--" placeholder="Время доставки">-->
<!--                   </td>-->
               </tr>
                   <? endforeach; ?>
          </table>
          <table class="table delivery">
            <tr class="delivery">
                <td>
                    <select  class="form-control" id="adrrdelivery" style="width:auto;">
                        <? foreach ( $stores3 as $store): ?>
                            <option value="<?=$store['ID']?>"><?=$store['NAME'].' '.$store['ADDRESS']?></option>
                        <? endforeach; ?>
                    </select>
                    <a href="javascript:void(0);" onclick="<?= $APPLICATION->GetPopupLink( array(
                            'URL' => 'https://10.97.21.13/crm/stores/0/ajaxedit/?return_url=/crm/deal/edit/'.$dealID['ID'].'/&consignee='.$consignee['UF_CRM_1566456322']['VALUE'].'',
                            'PARAMS' => array(
                                'width' => 780,
                                'height' => 570,
                                'resizable' => true,
                                'min_width' => 780,
                                'min_height' => 570
                            ))
                    ) ?>">
                        <span class="crm-offer-info-link pull-right">Добавить</span></a>
                </td>
                <td>
                   <span class="btn btn-success plus pull-left">+</span>

                </td>

            </tr>
            <tr>
            <td id="warning" hidden>
                <h2>Выберите товар сделки и нажмите кнопку применить ниже !</h2>
            </td>
            </tr>
        </table>


        <style type="text/css">
            .information_json, .information_json * {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
                background-color: transparent;
                border-spacing: 0;
                border-collapse: collapse;
            }
            .pull-right {float: left;}
            .pull-left {float: right;}
            .form-control {
                display: block;
                width: 100%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            .btn-danger {
                color: #fff;
                background-color: #d9534f;
                border-color: #d43f3a;
            }
            .btn-success {
                color: #fff;
                background-color: #5cb85c;
                border-color: #4cae4c;
            }
            .btn-link {
                font-weight: 400;
                color: #007bff;
                background-color: transparent;
            }
        </style>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script>// формируем новые поля
            var WEIGHT=null;
            var DATE=null;
            var TIME=null;
            var STORE=null;
            var ID=null;
            var total = 0;
            var UserfirldCompanyStore="UF_CRM_COMPANY_TEST";
            var dealID=<?=$consignee['UF_CRM_1566456322']['VALUE']?>;
            var PRODUCT_NAME=null;


              $('.plus').click(function(){


                var val = $("#adrrdelivery option:selected" ).val();
                let elem = document.getElementById('name['+val+']');

                  if(!elem) {
                      //var ids = save(null,"<?=self::getIDEdit() ?>", "<?= $consignee['UF_CRM_1566456322']['VALUE'];?>",val);
                      BX.rest.callMethod(
                          'crm.deliveryproduct.add',
                          {
                              fields: {
                                  'DEAL_ID': "<?=self::getIDEdit() ?>",
                                  'COMPANY_ID': "<?= $consignee['UF_CRM_1566456322']['VALUE'];?>",
                                  'STORE_ID': val,
                              },
                          },
                          function (result) {
                              var data = result.data();
                              $('.information_json_plus').before(
                                  '<tr>'+
                                  '<td><input type="hidden" name="<?= $fieldName ?>" class="form-control store" style="min-width: 400px;" id="name[' + val + ']" value="' + val + '" readonly >' +
                                  '<input type="text"  class="form-control text" style="min-width: 400px;" value="' + $("#adrrdelivery option:selected").text() + '" readonly >' +
                                  '</td>' +
                                  '<td><span class="btn btn-danger minus pull-right" data-id="' + data + '">&ndash;</span></td>' +
                                  '<td><input type="text" name="WEIGHT" data-id="' + data + '" class="form-control  weight" style="width: 50px;" id="information_json_val[w' + $("#adrrdelivery option:selected").val() + ']"  value="" placeholder="Вес в тоннах"></td>' +
                                  '<td><input type="date" name="DATE" required pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" data-id="' + data + '" class="form-control  date" style="width: 200px;" id="information_json_val[d' + $("#adrrdelivery option:selected").val() + ']"  value="" placeholder="Срок доставки"></td>' +
                                //  '<td><input type="time" name="TIME" data-id="' + data + '" class="form-control time" style="width: 100px;" id="information_json_val[t' + $("#adrrdelivery option:selected").val() + ']"  value="" placeholder="Время доставки"></td>'+
                                  '</tr>'
                              );

                          }
                      );
                  }else{

                      alert("Такой пункт разгрузки уже выбран!");
                  }
            });


            // BX.rest.callMethod(
            //     'crm.company.list',
            //     {
            //         filter:{"=ID": dealID},
            //         select: [UserfirldCompanyStore]
            //     },
            //     function (result) {
            //        let arr =result.data();
            //
            //             BX.rest.callMethod(
            //                 'crm.store.list',
            //                 {
            //                     filter:{"=ID": arr[0][UserfirldCompanyStore]},
            //                     select: [ "ID","NAME","ADDRESS"]
            //                 },
            //                 function (results) {
            //                   let dataArray =results.data();
            //                     let dropdown = $('#adrrdelivery');
            //                     dropdown.empty();
            //                    // dropdown.append('<option>(Не задано)</option>');
            //                     dropdown.prop('selectedIndex', 0);
            //                     for (var data in dataArray) {
            //                       //  console.log(dataArray[data].ID);
            //                          dropdown.append($('<option></option>').attr('value', dataArray[data].ID).text(dataArray[data].NAME+' '+dataArray[data].ADDRESS));
            //                     }
            //                 });
            //
            //     });

            $(document).on('change',function(evt){

                total = 0;
                $(this).find("input.weight").each(function() {
                    total += parseInt($(this).val());
                    quantity=$('#deal_<?=self::getIDEdit() ?>_product_editor_product_row_0_QUANTITY').val();
                    PRODUCT_NAME=$('#deal_<?=self::getIDEdit() ?>_product_editor_product_row_0_PRODUCT_NAME').val();
                    console.log(PRODUCT_NAME);
                });
                if (total > quantity && evt!=="load") {
                    alert("Вес превышен от на" + (quantity - total) + ".т !");
                }
                if (total < quantity && evt!=="load") {
                    alert("Веса не хватает на" + (quantity - total) + " .т !");
                }


                if (quantity == null) {
                    $("#warning").show();
                    $(".plus").hide();

                }else {
                    $(".plus").show();
                    $("#warning").hide();
                }




            });


            $(document).on('change','input.form-control', function (e) {
                     if(this.attributes['data-id']) {
                        ID=this.attributes['data-id'].nodeValue;

                      }

                STORE=store_id(this.id);

                if(this.name==='WEIGHT') {
                    WEIGHT= $(this).val();


                }
                if(this.name==='DATE') {
                    DATE= $(this).val();
                }
                if(this.name==='TIME') {
                    TIME= $(this).val();
                }

                if(STORE!==null) {
                    save(ID,"<?=self::getIDEdit() ?>", "<?= $consignee['UF_CRM_1566456322']['VALUE'];?>",PRODUCT_NAME,STORE, DATE, TIME, WEIGHT);
                }
                // console.log(ID);
                // console.log(STORE);
                // console.log(WEIGHT);
                // console.log(DATE);
                // console.log(TIME);

            });


            function save(ID,DEAL_ID,COMPANY_ID,PRODUCT_NAME,STORE_ID,DATE_DELIVERY,TIME_DELIVERY,WEIGHT) {

                BX.rest.callMethod(
                    'crm.deliveryproduct.list',
                    {
                        filter:{"=DEAL_ID":DEAL_ID,
                            "=COMPANY_ID": COMPANY_ID,
                            "=STORE_ID": STORE_ID,},
                        select: [ "ID"]
                    },
                    function (result) {
                     //   console.log(result.data());

              if (result.data().length !== 0) {
                    BX.rest.callMethod(
                        'crm.deliveryproduct.update',
                        {
                            "ID": ID,
                            fields: {
                                'DEAL_ID': DEAL_ID,
                                'COMPANY_ID': COMPANY_ID,
                                'PRODUCT_NAME':PRODUCT_NAME,
                                'STORE_ID': STORE_ID,
                                'DATE_DELIVERY': DATE_DELIVERY,
                                'TIME_DELIVERY': TIME_DELIVERY,
                                'WEIGHT': WEIGHT
                            },
                        },
                        function (result) {
                  //      console.log(result);

                        }
                    );
              }
                    }
                );


            }

            function store_id($value){
                var str=$value;
                return str.replace(/[^0-9]/gim,'');
            }


            // on - так как элемент динамически создан и обычный обработчик с ним не работает
            $(document).on('click', '.minus', function(){
                if (confirm("Вы - уверены что хотите удалсть пункт разгрузки?"))
                    if(this.attributes['data-id']) {

                        BX.rest.callMethod(
                            'crm.deliveryproduct.delete',
                            {
                                "id":this.attributes['data-id'].nodeValue
                            },
                            function (result) {
                           //     console.log(result);
                            }
                        );
                       // console.log(this.attributes['data-id'].nodeValue);
                    }
                $( this ).closest( 'tr' ).remove(); // удаление строки с полями
            });
        </script>

    <?endif;?>



        <?
             $selectorHtml = ob_get_clean();

            return $selectorHtml;
        }


    private static function getStoreLink($storeId)
    {

        $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", self::getIDEdit());
        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
            'filter' => array(
                'ID' => $consignee['UF_CRM_1566456322']['VALUE']
            )
        ));
        $stores2 = $dbCompany->fetchAll();
        $dbStores1 = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => $stores2[0]['UF_CRM_COMPANY_TEST'])
            ));
        $stores3 = $dbStores1->fetchAll();
        $param = implode(",", $consignee['UF_CRM_STORE_DEAL']['VALUE']);
        $dbStores = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => explode(',', $param))
            ));
        $stores = $dbStores->fetchAll();

        $delivery=DeliveryProductTable::getList(
            array('select' => array('*'),
                'filter' => array("=COMPANY_ID" =>$consignee['UF_CRM_1566456322']['VALUE'],"DEAL_ID"=>self::getIDEdit())
            )
        );
        $delivery_result=$delivery->fetchAll();


  ?>


         <table class="table information_json" border="1">
            <tr>
                <th>Пункт разгрузки</th>
                <th>Вес</th>
                <th>Дата поставки</th>
<!--                <th>Время поставки</th>-->
            </tr>
               <tr class="information_json_plus">

               </tr>
                   <? foreach ($delivery_result as $delivery):
                       $storeDetailTemplate = Option::get('academy.crmstores', 'STORE_DETAIL_TEMPLATE');
                       $storeUrl = \CComponentEngine::makePathFromTemplate(
                           $storeDetailTemplate,
                           array('STORE_ID' =>$delivery['STORE_ID'])
                       );
                   ?>
               <tr>
                   <td  class="form-control name" style="min-width: 400px;" >
<!--                   <input type="text"  class="form-control name" style="min-width: 400px;"  value="--><?//=self::getStoreInfi($delivery['STORE_ID'])?><!--" readonly >-->
                  <a class="feed-com-add-link" href="<?=htmlspecialcharsbx($storeUrl)?>"><?=self::getStoreInfi($delivery['STORE_ID'])?></a>
                   </td>
                       <td><input type="text" name="WEIGHT" data-id="<?= $delivery['ID']?>" class="form-control  weight" style="width: 50px;" id="information_json_val[w<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['WEIGHT']?>" placeholder="Вес в тоннах" readonly ></td>
                       <td><input type="text" name="DATE" data-id="<?=  $delivery['ID']?>" class="form-control  date" style="width: 200px;" id="information_json_val[d<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['DATE_DELIVERY']?>" placeholder="Дата доставки" readonly ></td>
<!--                       <td><input type="time" name="TIME" data-id="--><?//= $delivery['ID']?><!--" class="form-control time" style="width: 100px;" id="information_json_val[t--><?//=$delivery['STORE_ID']?><!--]"  value="--><?//= $delivery['TIME_DELIVERY']?><!--" placeholder="Время доставки" readonly ></td>-->
               </tr>
                   <? endforeach; ?>

        </table>
<?

        return false;

    }

    static function GetListValues($arValues){
        $result=[];
        if(is_array($arValues['ID'])&& count($arValues['ID'])>0){
            $query = StoreTable::getlist([
                'filter'=>[
                    '=ID'=>$arValues['ID']
                ],
                'select'=>[
                    'ID',
                    'NAME',
                ]
            ]);
            while($data = $query->fetch()){
                $result[$data['ID']]['VALUE'] = $data['NAME'];
            }
        }
        return $result;
    }

    private static function getIDEdit()
    {
        $request =Application::getInstance()->getContext()->getRequest();
        $uriString = $request->getRequestUri();
        $uri = new Uri($uriString);
        $redirect = preg_replace('~\D+~','',$uri->getUri());

        return  $redirect;
    }

    private static function getStoreInfi($id){

        $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", self::getIDEdit());

        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
            'filter' => array(
                'ID' => $consignee['UF_CRM_1566456322']['VALUE']
            )
        ));

        $stores2 = $dbCompany->fetchAll();
        $dbStores1 = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => $id)
            ));
        $stores3 = $dbStores1->fetchAll();


       return  $stores3[0]['NAME'].'  '.$stores3[0]['ADDRESS'];
    }
}