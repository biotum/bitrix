<?php

namespace Academy\CrmStores\UserType;


use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Crm\DealRecurTable;
use Bitrix\Crm\DealTable;
use Bitrix\Crm\Entity\Deal;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserField\TypeBase;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Web\Uri;
use CCrmBizProc;
use CCrmComponentHelper;
use CCrmDeal;
use CCrmOwnerType;
use CCrmPerms;
use CCrmUserType;


class StoreBinding extends TypeBase
{
    const USER_TYPE_ID = 'storebinding';

    function GetUserTypeDescription ()
    {
        return array(
            'USER_TYPE_ID' => static::USER_TYPE_ID,
            'CLASS_NAME' => __CLASS__,
            'DESCRIPTION' => Loc::getMessage('CRMSTORES_STOREBINDING'),
            'BASE_TYPE' => \CUserTypeManager::BASE_TYPE_INT,
            'EDIT_CALLBACK' => array(__CLASS__, 'GetPublicEdit'),
            'VIEW_CALLBACK' => array(__CLASS__, 'GetPublicView'),

        );
    }

    function GetDBColumnType ($arUserField)
    {
        global $DB;
        switch(strtolower($DB->type))
        {
            case "mysql":
                return "int(18)";
            case "oracle":
                return "number(18)";
            case "mssql":
                return "int";
        }
        return "int";
    }

    function GetFilterHTML($arUserField, $arHtmlControl)
    {
        return sprintf(
            '<input type="text" name="%s" size="%s" value="%s">',
            $arHtmlControl['NAME'],
            $arUserField['SETTINGS']['SIZE'],
            $arHtmlControl['VALUE']
        );
    }

    function GetFilterData($arUserField, $arHtmlControl)
    {
        return array(
            'id' => $arHtmlControl['ID'],
            'name' => $arHtmlControl['NAME'],
            'filterable' => ''
        );
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        return !empty($arHtmlControl['VALUE']) ? self::getStoreLink($arHtmlControl['VALUE']) : '&nbsp;';
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    public static function GetPublicView($arUserField, $arAdditionalParameters = array())
    {
        return !empty($arUserField['VALUE']) ? self::getStoreLink($arUserField['VALUE']) : '&nbsp;';
    }

    public static function GetPublicEdit($arUserField, $arAdditionalParameters = array())
    {
        $fieldName = static::getFieldName($arUserField, $arAdditionalParameters);
        $value = static::getFieldValue($arUserField, $arAdditionalParameters);
        $value = reset($value);


        return self::getStoreSelector($fieldName, $value);
    }

    function OnSearchIndex($arUserField)
    {
        if(is_array($arUserField["VALUE"]))
            return implode("\r\n", $arUserField["VALUE"]);
        else
            return $arUserField["VALUE"];
    }

    private static function getStoreSelector($fieldName, $fieldValue = [])
    {
        if (!Loader::includeModule('academy.crmstores')) {
            return '';
        }

//        $company=\CModule::IncludeModule('crm');
//
//        $rootActivity = $company->GetRootActivity();

//
//        $Company = $rootActivity->GetVariable("COMPANY");

         global $APPLICATION;
        $dealID = CCrmDeal::GetByID(self::getIDEdit());

        if(empty($dealID)){
          $company=\CCrmCompany::GetByID(self::getIDEdit());
            $dealID['COMPANY_ID']= $company['ID'];
        }



    //print_r($_SESSION['LOCAL_REDIRECTS']['R']);
        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
            'filter' => array(
                'ID' => $dealID['COMPANY_ID']
            )
        ));

        $stores2 = $dbCompany->fetchAll();

        $param = implode(",", $stores2[0]['UF_CRM_COMPANY_TEST']);

            $dbStores = StoreTable::getList(
                array('select' => array('ID', 'NAME','ADDRESS'),
                 'filter' => array("=ID" => explode(',', $param))
                ));

        $stores = $dbStores->fetchAll();

        $dbStoreslist = StoreTable::getList( array('select' => array('ID', 'NAME','ADDRESS')));
        $liststore=$dbStoreslist->fetchAll();
        $isNoValue = $fieldValue === null;

        ob_start();
       ?>


        <? if(!CCrmDeal::GetByID(self::getIDEdit())):?>

        <table class="table information_json">
            <tr>
                <th>Пункт разгрузки</th>
                <th></th>
                <th></th>
            </tr>
            <tr class="information_json_plus">
                <td></td>
                <td></td>

                <? foreach ($stores as $store): ?>
                <tr>
                <td><input type="hidden" name="<?= $fieldName ?>" class="form-control name" style="min-width: 400px;" id="name[<?=$store['ID']?>]" value="<?=$store['ID']?>" readonly >
                    <input type="text"  class="form-control name" style="min-width: 400px; background-color: #ecf1f2;"  value="<?=$store['NAME'].' '.$store['ADDRESS']?>" readonly >
                </td>
                <td></td>
                </tr>
            <? endforeach; ?>

        </table>
         <table class="table">
            <tr>
                <td>

                    <select   class="form-control" id="adrrdelivery" style="width:300px;">
                        <? foreach ( $liststore as $store): ?>

                            <option value="<?=$store['ID']?>"><?=$store['NAME'].' '.$store['ADDRESS']?></option>
                        <? endforeach; ?>
                    </select>

                    <a href="javascript:void(0);" onclick="<?= $APPLICATION->GetPopupLink( array(
                            'URL' => 'https://10.97.21.13/crm/stores/0/ajaxedit/?return_url=/crm/company/edit/'.$dealID['COMPANY_ID'],
                            'PARAMS' => array(
                                'width' => 780,
                                'height' => 570,
                                'resizable' => true,
                                'min_width' => 780,
                                'min_height' => 570
                            ))
                    ) ?>"><span class="crm-offer-info-link pull-right">Добавить</span></a>

                </td>

                <td>
                   <span class="btn btn-success plus pull-left">+</span>

                </td>


            </tr>
        </table>

        <style type="text/css">
            .information_json, .information_json * {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }
            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
                background-color: transparent;
                border-spacing: 0;
                border-collapse: collapse;
            }
            .pull-right {float: left;}
            .form-control {
                display: block;
                width: 100%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            }
            .btn {
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }
            .btn-danger {
                color: #fff;
                background-color: #d9534f;
                border-color: #d43f3a;
            }
            .btn-success {
                color: #fff;
                background-color: #5cb85c;
                border-color: #4cae4c;
            }
        </style>

        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>

        <script>

                    // BX.rest.callMethod(
                    //     'crm.store.list',
                    //     {
                    //         select: [ "ID","NAME","ADDRESS"]
                    //     },
                    //     function (results) {
                    //         let dataArray =results.data();
                    //         let dropdown = $('#adrrdelivery');
                    //         dropdown.empty();
                    //         dropdown.append('<option>(Не задано)</option>');
                    //         dropdown.prop('selectedIndex', 0);
                    //         for (var dat in dataArray) {
                    //
                    //           //  dropdown.append($('<option></option>').attr('value', dataArray[dat].ID).text(dataArray[dat].NAME+' '+dataArray[dat].ADDRESS));
                    //         }
                    //     });


            $('.plus').click(function(){
                 var val = $("#adrrdelivery option:selected" ).val();
                let elem = document.getElementById('name['+val+']');
                if(!elem){
                $('.information_json_plus').before(
                    '<tr>' +
                    '<td><input type="hidden" name="<?= $fieldName ?>" class="form-control name" style="min-width: 400px;" id="name['+val+']" value="'+val+'" readonly >' +
                    '<input type="text"  class="form-control text" style="min-width: 400px; background-color: #ecf1f2;" value="'+$("#adrrdelivery option:selected" ).text()+'" readonly >' +
                    '</td>' +
                    '<td><span class="btn btn-danger minus pull-right">&ndash;</span></td>' +
                    '</tr>'
                );
              }else{

                    alert("Такой пункт разгрузки уже привязан!");
                }
            });
            // on - так как элемент динамически создан и обычный обработчик с ним не работает
            //$(document).on('click', '.minus', function(){
            //    $( this ).closest( 'tr' ).remove(); // удаление строки с полями
            //    $('.information_json_plus').before('<td><input type="hidden" name="<?//= $fieldName ?>//" class="form-control name" style="min-width: 400px;" id="name[]" value="" readonly >');
            //});
        </script>


    <?endif;?>


        <?
             $selectorHtml = ob_get_clean();

            return $selectorHtml;
        }


    private static function getStoreLink($storeId)
    {
        $href='';

        if (!Loader::includeModule('academy.crmstores')) {
            return '';
        }

        foreach ($storeId as $item=>$value) {
            $dbStore = StoreTable::getById($value);
            $store = $dbStore->fetch();
        if (empty($store)) {
            return '';
        }
            $storeDetailTemplate = Option::get('academy.crmstores', 'STORE_DETAIL_TEMPLATE');
            $storeUrl = \CComponentEngine::makePathFromTemplate(
                $storeDetailTemplate,
                array('STORE_ID' => $store['ID'])
            );
           echo '<div style=" border: 1px solid black;"><a href="' . htmlspecialcharsbx($storeUrl) . '">' . htmlspecialcharsbx($store['NAME']) . ' ' . htmlspecialcharsbx($store['ADDRESS']) . '</a></div>';

        }


        return false;

    }

    static function GetListValues($arValues){
        $result=[];
        if(is_array($arValues['ID'])&& count($arValues['ID'])>0){
            $query = StoreTable::getlist([
                'filter'=>[
                    '=ID'=>$arValues['ID']
                ],
                'select'=>[
                    'ID',
                    'NAME',
                ]
            ]);
            while($data = $query->fetch()){
                $result[$data['ID']]['VALUE'] = $data['NAME'];
            }
        }
        return $result;
    }

    private static function getIDEdit()
    {
        $uri = new Uri($_SESSION['LOCAL_REDIRECTS']['R']);
        $redirect = preg_replace('~\D+~','',$uri->getPath());
        return  $redirect;
    }

}