<?php

namespace Academy\CrmStores\UserType;


use Academy\CrmStores\Entity\DeliveryProductTable;
use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Crm\DealRecurTable;
use Bitrix\Crm\DealTable;
use Bitrix\Crm\Entity\Deal;
use Bitrix\Intranet\Integration\Main;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Context;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\DateField;
use Bitrix\Main\UserField\TypeBase;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Web\Uri;
use Bitrix\Tasks\Util\Entity\DateTimeField;
use CBitrixComponent;
use CCrmBizProc;
use CCrmComponentHelper;
use CCrmDeal;
use CCrmOwnerType;
use CCrmPerms;
use CCrmUserType;

use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\UserTable;

class StoreBindingdeal extends TypeBase
{
    const USER_TYPE_ID = 'storebindingdeal';

    function GetUserTypeDescription ()
    {
        return array(
            'USER_TYPE_ID' => static::USER_TYPE_ID,
            'CLASS_NAME' => __CLASS__,
            'DESCRIPTION' => Loc::getMessage('CRMSTORES_STOREBINDINGDEAL'),
            'BASE_TYPE' => \CUserTypeManager::BASE_TYPE_INT,
            'EDIT_CALLBACK' => array(__CLASS__, 'GetPublicEdit'),
            'VIEW_CALLBACK' => array(__CLASS__, 'GetPublicView'),

        );
    }

    function GetDBColumnType ($arUserField)
    {
        global $DB;
        switch(strtolower($DB->type))
        {
            case "mysql":
                return "int(18)";
            case "oracle":
                return "number(18)";
            case "mssql":
                return "int";
        }
        return "int";
    }

    function GetFilterHTML($arUserField, $arHtmlControl)
    {
        return sprintf(
            '<input type="text" name="%s" size="%s" value="%s">',
            $arHtmlControl['NAME'],
            $arUserField['SETTINGS']['SIZE'],
            $arHtmlControl['VALUE']
        );
    }

    function GetFilterData($arUserField, $arHtmlControl)
    {
        return array(
            'id' => $arHtmlControl['ID'],
            'name' => $arHtmlControl['NAME'],
            'filterable' => ''
        );
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        return !empty($arHtmlControl['VALUE']) ? self::getStoreLink($arHtmlControl['VALUE']) : '&nbsp;';
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        return self::getStoreSelector($arHtmlControl["NAME"], $arHtmlControl["VALUE"]);
    }

    public static function GetPublicView($arUserField, $arAdditionalParameters = array())
    {
        return !empty($arUserField['VALUE']) ? self::getStoreLink($arUserField['VALUE']) : '&nbsp;';
    }

    public static function GetPublicEdit($arUserField, $arAdditionalParameters = array())
    {
        $fieldName = static::getFieldName($arUserField, $arAdditionalParameters);
        $value = static::getFieldValue($arUserField, $arAdditionalParameters);
        $value = reset($value);


        return self::getStoreSelector($fieldName, $value);
    }

    function OnSearchIndex($arUserField)
    {
        if(is_array($arUserField["VALUE"]))
            return implode("\r\n", $arUserField["VALUE"]);
        else
            return $arUserField["VALUE"];
    }

    private static function getStoreSelector($fieldName, $fieldValue = null)
    {
        if (!Loader::includeModule('academy.crmstores')) {
            return '';
        }

        ob_start();

      ?>
        <? global $APPLICATION;
        $APPLICATION->IncludeComponent(
        "academy.crmstores:store.deliveryedit",
        ".default",
        Array(
        ),
        false
        );
        ?>

        <?
             $selectorHtml = ob_get_clean();

            return $selectorHtml;
        }


    private static function getStoreLink($storeId)
    {

        $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", self::getIDEdit());
        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
            'filter' => array(
                'ID' => $consignee['UF_CRM_1566456322']['VALUE']
            )
        ));
        $stores2 = $dbCompany->fetchAll();
        $dbStores1 = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => $stores2[0]['UF_CRM_COMPANY_TEST'])
            ));
        $stores3 = $dbStores1->fetchAll();
        $param = implode(",", $consignee['UF_CRM_STORE_DEAL']['VALUE']);
        $dbStores = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => explode(',', $param))
            ));
        $stores = $dbStores->fetchAll();

        $delivery=DeliveryProductTable::getList(
            array('select' => array('*'),
                'filter' => array("=COMPANY_ID" =>$consignee['UF_CRM_1566456322']['VALUE'],"DEAL_ID"=>self::getIDEdit())
            )
        );
        $delivery_result=$delivery->fetchAll();


  ?>


         <table class="table information_json" border="1">
            <tr>
                <th>Пункт разгрузки</th>
                <th>Вес</th>
                <th>Дата поставки</th>
                <th>Время поставки</th>
            </tr>
               <tr class="information_json_plus">

               </tr>
                   <? foreach ($delivery_result as $delivery):
                       $storeDetailTemplate = Option::get('academy.crmstores', 'STORE_DETAIL_TEMPLATE');
                       $storeUrl = \CComponentEngine::makePathFromTemplate(
                           $storeDetailTemplate,
                           array('STORE_ID' =>$delivery['STORE_ID'])
                       );
                   ?>
               <tr>
                   <td  class="form-control name" style="min-width: 400px;" >
<!--                   <input type="text"  class="form-control name" style="min-width: 400px;"  value="--><?//=self::getStoreInfi($delivery['STORE_ID'])?><!--" readonly >-->
                  <a class="feed-com-add-link" href="<?=htmlspecialcharsbx($storeUrl)?>"><?=self::getStoreInfi($delivery['STORE_ID'])?></a>
                   </td>
                       <td><input type="text" name="WEIGHT" data-id="<?= $delivery['ID']?>" class="form-control  weight" style="width: 50px;" id="information_json_val[w<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['WEIGHT']?>" placeholder="Вес в тоннах" readonly ></td>
                       <td><input type="date" name="DATE" data-id="<?=  $delivery['ID']?>" class="form-control  date" style="width: 200px;" id="information_json_val[d<?=$delivery['STORE_ID']?>]"  value="<?=$delivery['DATE_DELIVERY']?>" placeholder="Дата доставки" readonly ></td>
                       <td><input type="time" name="TIME" data-id="<?= $delivery['ID']?>" class="form-control time" style="width: 100px;" id="information_json_val[t<?=$delivery['STORE_ID']?>]"  value="<?= $delivery['TIME_DELIVERY']?>" placeholder="Время доставки" readonly ></td>
               </tr>
                   <? endforeach; ?>

        </table>
<?

        return false;

    }

    static function GetListValues($arValues){
        $result=[];
        if(is_array($arValues['ID'])&& count($arValues['ID'])>0){
            $query = StoreTable::getlist([
                'filter'=>[
                    '=ID'=>$arValues['ID']
                ],
                'select'=>[
                    'ID',
                    'NAME',
                ]
            ]);
            while($data = $query->fetch()){
                $result[$data['ID']]['VALUE'] = $data['NAME'];
            }
        }
        return $result;
    }

    private static function getIDEdit()
    {
        $request =Application::getInstance()->getContext()->getRequest();
        $uriString = $request->getRequestUri();
        $uri = new Uri($uriString);
        $redirect = preg_replace('~\D+~','',$uri->getUri());

        return  $redirect;
    }

    private static function getStoreInfi($id){

        $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", self::getIDEdit());

        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
            'filter' => array(
                'ID' => $consignee['UF_CRM_1566456322']['VALUE']
            )
        ));

        $stores2 = $dbCompany->fetchAll();
        $dbStores1 = StoreTable::getList(
            array('select' => array('ID', 'NAME','ADDRESS'),
                'filter' => array("=ID" => $id)
            ));
        $stores3 = $dbStores1->fetchAll();


       return  $stores3[0]['NAME'].'  '.$stores3[0]['ADDRESS'];
    }
}